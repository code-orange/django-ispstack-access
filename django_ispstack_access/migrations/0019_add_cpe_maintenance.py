# Generated by Django 3.2 on 2021-04-17 14:50

from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):
    dependencies = [
        ("django_ispstack_access", "0018_add_best_product"),
    ]

    operations = [
        migrations.CreateModel(
            name="IspAccessCpeMaintenance",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("comment", models.CharField(max_length=250)),
                ("contact_provider_fn", models.CharField(max_length=30)),
                ("contact_provider_ln", models.CharField(max_length=30)),
                (
                    "contact_provider_tel",
                    phonenumber_field.modelfields.PhoneNumberField(
                        max_length=128, region=None
                    ),
                ),
                ("contact_provider_email", models.CharField(max_length=250)),
                (
                    "contact_provider_fax",
                    phonenumber_field.modelfields.PhoneNumberField(
                        max_length=128, region=None
                    ),
                ),
                (
                    "cpe",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_ispstack_access.ispaccesscpe",
                    ),
                ),
            ],
            options={
                "db_table": "isp_access_cpe_maintenance",
            },
        ),
    ]
