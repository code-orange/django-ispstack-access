from pprint import pprint

import slumber
from django.conf import settings
from django.core.management.base import BaseCommand
from django.template import engines

from dit_enterprisehub_sap.models import *


def xstr(s):
    if s is None:
        return ""
    return str(s)


class Command(BaseCommand):
    help = "Sends email notifications for new access product installation"

    def handle(self, *args, **options):
        api = slumber.API(
            settings.ZAMMAD_URL, auth=(settings.ZAMMAD_USER, settings.ZAMMAD_PASSWD)
        )

        txt_ticketnote_text = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_access_prod_ticket_note"
        )
        txt_ticketnote_footer = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_footer"
        )
        txt_ticketnote = "\r\n".join(
            [txt_ticketnote_text.u_template, txt_ticketnote_footer.u_template]
        )

        txt_ticketmail_header = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_header"
        )
        txt_ticketmail_text = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_access_prod_schaltung"
        )
        txt_ticketmail_footer = DolMainTemplates.objects.using("sapserver").get(
            name="email_template_footer"
        )
        txt_ticketmail = "\r\n".join(
            [
                txt_ticketmail_header.u_template,
                txt_ticketmail_text.u_template,
                txt_ticketmail_footer.u_template,
            ]
        )

        django_engine = engines["django"]

        # New Installation Date
        self.stdout.write("New Port Requests")

        all_open_inst = (
            DolTelAccessNtfr.objects.using("sapserver")
            .exclude(u_accessinstid__u_custnr__e_mail=None)
            .exclude(u_accessinstid__u_inst_date="2999-01-01 00:00:00")
            .filter(u_sentinstant="2999-01-01 00:00:00")
            .select_related()
            .order_by("u_accessinstid__u_inst_date")
        )

        print(all_open_inst.query)

        for p in all_open_inst:
            print("============================")
            print(p.u_accessinstid.u_custnr.cardcode)
            print(p.u_accessinstid.u_custnr.cardname)
            print(p.u_accessinstid.u_custnr.e_mail)
            print(p.u_accessinstid.u_contract_nr)
            print("============================")

            template_opts = dict()
            template_opts["customer_code"] = p.u_accessinstid.u_custnr.cardcode
            template_opts["customer_name"] = p.u_accessinstid.u_custnr.cardname
            template_opts["customer_email"] = p.u_accessinstid.u_custnr.e_mail
            template_opts["access_inst_date"] = p.u_accessinstid.u_inst_date.strftime(
                "%d.%m.%Y"
            )
            template_opts["access_inst_timeframe"] = p.u_accessinstid.u_timeframe.name
            template_opts["access_inst_adrlines"] = (
                xstr(p.u_accessinstid.u_adrline1)
                + " "
                + xstr(p.u_accessinstid.u_adrline2)
            )
            template_opts["access_inst_adrcountry"] = xstr(
                p.u_accessinstid.u_adrcountry.name
            )
            template_opts["access_inst_adrzipcode"] = xstr(
                p.u_accessinstid.u_zipcode.u_zipcode
            )
            template_opts["access_inst_adrzipname"] = xstr(
                p.u_accessinstid.u_zipcode.name
            )
            template_opts["access_inst_adrstreet"] = xstr(p.u_accessinstid.u_adrstreet)
            template_opts["access_inst_adrhousenr"] = xstr(
                p.u_accessinstid.u_adrhousenr
            )
            template_opts["access_inst_adrhousenradd"] = xstr(
                p.u_accessinstid.u_adrhousenradd
            )

            ticket_title = (
                "Ihr neuer Dolphin Connect-Anschluss (CPE "
                + str(p.u_accessinstid.u_contract_nr)
                + ")"
            )

            if p.u_accessinstid.u_ticketid is None:
                template_ticketnote = django_engine.from_string(txt_ticketnote)

                ticketdata = dict()
                ticketdata["title"] = ticket_title
                ticketdata["group"] = 1
                ticketdata["customer_id"] = "guess:" + p.u_accessinstid.u_custnr.e_mail
                ticketdata["article"] = dict()
                ticketdata["article"]["subject"] = ticket_title
                ticketdata["article"]["body"] = template_ticketnote.render(
                    template_opts
                )
                ticketdata["article"]["type"] = "note"
                ticketdata["article"]["internal"] = False
                ticketdata["note"] = ticket_title

                ticket = api.tickets.post(ticketdata)

                p.u_accessinstid.u_ticketid = ticket["id"]
                p.u_accessinstid.save()
                pprint(ticket["id"])

            template_ticketmail = django_engine.from_string(txt_ticketmail)
            articledata = dict()
            articledata["ticket_id"] = p.u_accessinstid.u_ticketid
            articledata["to"] = p.u_address
            articledata["subject"] = ticket_title
            articledata["body"] = template_ticketmail.render(template_opts)
            articledata["content_type"] = "text/plain"
            articledata["type"] = "email"
            articledata["internal"] = False

            article = api.ticket_articles.post(articledata)

            # set new info
            p.u_sentinstant = p.u_accessinstid.u_inst_date
            p.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
