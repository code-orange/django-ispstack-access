import ldap3
from celery import shared_task
from django.db import transaction, connections
from django.forms import model_to_dict
from django.template import engines
from django_python3_ldap.conf import settings as ldap_settings
from ldap3 import BASE, ALL_ATTRIBUTES, MODIFY_REPLACE
from ldap3.core.exceptions import LDAPNoSuchObjectResult

from django_freeradius_models.django_freeradius_models import models as freeradius
from django_ispstack_access.django_ispstack_access.func import *
from django_ispstack_access.django_ispstack_access.models import *
from django_simple_notifier.django_simple_notifier.models import *
from django_simple_notifier.django_simple_notifier.plugin_zammad import *


@shared_task(name="ispstack_access_radius_auth")
def ispstack_access_radius_auth():
    jobs_unprocessed = IspAccessCpe.objects.exclude(
        ispaccesscpeoptions__attribute__name__in=("auth_username", "auth_password"),
    ).filter(
        product__ispaccesscarrierlinkoptions__attribute__name="auth_radius_style",
    )

    for job in jobs_unprocessed:
        with transaction.atomic():
            radius_domain = job.product.ispaccesscarrierlinkoptions_set.get(
                attribute__name="auth_radius_domain"
            ).value

            # TODO: fix customer_id
            radius_username_prefix = IspAccessMdatOptions.objects.get(
                customer_id=settings.MDAT_ROOT_CUSTOMER_ID,
                attribute__name="radius_username_prefix",
            ).value

            radius_username = generate_radius_username(
                job.customer, radius_username_prefix, radius_domain
            )
            radius_password = random_password(16)

            add_cpe_option(job, "auth_username", radius_username)
            add_cpe_option(job, "auth_password", radius_password)

    return


@shared_task(name="ispstack_access_sync_freeradius")
def ispstack_access_sync_freeradius():
    # Connect to DBs
    freeradius_db = IspAccessFreeradiusDb.objects.all().first()

    database = freeradius_db.name

    new_database = dict()
    new_database["id"] = database
    new_database["NAME"] = freeradius_db.db_dbase
    new_database["CONN_MAX_AGE"] = 30
    new_database["ENGINE"] = "django.db.backends.mysql"
    new_database["USER"] = freeradius_db.db_user
    new_database["PASSWORD"] = freeradius_db.db_passwd
    new_database["HOST"] = freeradius_db.db_ip
    new_database["PORT"] = "3306"
    new_database["OPTIONS"] = {"init_command": "SET sql_mode='STRICT_TRANS_TABLES'"}
    connections.databases[database] = new_database

    all_accounts = IspAccessCpe.objects.all()

    for account in all_accounts:
        local_username = account.ispaccesscpeoptions_set.get(
            attribute__name="auth_username"
        ).value
        local_password = account.ispaccesscpeoptions_set.get(
            attribute__name="auth_password"
        ).value

        try:
            radius_check = freeradius.Radcheck.objects.using(database).get(
                username=local_username
            )
        except freeradius.Radcheck.DoesNotExist:
            radius_check = freeradius.Radcheck(username=local_username)

        radius_check.attribute = "Cleartext-Password"
        radius_check.op = ":="
        radius_check.value = local_password

        radius_check.save(using=database)

    all_accounts_remote = freeradius.Radcheck.objects.using(database).all()

    for account_remote in all_accounts_remote:
        try:
            account_local = IspAccessCpeOptions.objects.get(
                attribute__name="auth_username", value=account_remote.username
            )
        except IspAccessCpeOptions.DoesNotExist:
            account_remote.delete(using=database)

    return


@shared_task(name="ispstack_access_sync_freeradius_ldap")
def ispstack_access_sync_freeradius_ldap():
    # Configure the connection.
    if ldap_settings.LDAP_AUTH_USE_TLS:
        auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
    else:
        auto_bind = ldap3.AUTO_BIND_NO_TLS

    ldap_connection = ldap3.Connection(
        ldap3.Server(
            ldap_settings.LDAP_AUTH_URL,
            allowed_referral_hosts=[("*", True)],
            get_info=ldap3.NONE,
            connect_timeout=ldap_settings.LDAP_AUTH_CONNECT_TIMEOUT,
        ),
        user=ldap_settings.LDAP_AUTH_CONNECTION_USERNAME,
        password=ldap_settings.LDAP_AUTH_CONNECTION_PASSWORD,
        auto_bind=auto_bind,
        raise_exceptions=True,
        receive_timeout=ldap_settings.LDAP_AUTH_RECEIVE_TIMEOUT,
    )

    all_accounts = IspAccessCpe.objects.all()

    for account in all_accounts:
        tenant_ou = (
            "ou=" + account.customer.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE
        )

        local_username = account.ispaccesscpeoptions_set.get(
            attribute__name="auth_username"
        ).value
        local_password = account.ispaccesscpeoptions_set.get(
            attribute__name="auth_password"
        ).value

        cred_dn = "cn=" + local_username + ",OU=Credentials,OU=WAN," + tenant_ou

        print("DN: " + cred_dn)

        try:
            ldap_connection.search(
                search_base=cred_dn,
                search_filter="(&(objectClass=radiusprofile)(codeorange-radius-tag=wan))",
                search_scope=BASE,
                attributes=ALL_ATTRIBUTES,
            )
        except LDAPNoSuchObjectResult:
            # new credentials
            ldap_connection.add(
                dn=cred_dn,
                attributes={
                    "objectClass": ["radiusprofile", "account"],
                    "description": local_username,
                    "name": local_username,
                    "uid": local_username,
                    "dialupAccess": "yes",
                    "codeorange-radius-tag": "wan",
                    "codeorange-userPassword": local_password,
                },
            )

        ldap_connection.modify(
            dn=cred_dn,
            changes={
                "objectClass": [(MODIFY_REPLACE, ["radiusprofile", "account"])],
                "description": [(MODIFY_REPLACE, [local_username])],
                "uid": [(MODIFY_REPLACE, [local_username])],
                "dialupAccess": [(MODIFY_REPLACE, ["yes"])],
                "codeorange-radius-tag": [(MODIFY_REPLACE, ["wan"])],
                "codeorange-userPassword": [(MODIFY_REPLACE, [local_password])],
            },
        )

        # TODO: cleanup / remove old accounts that don't exist in ISPStack (anymore)

    return


@shared_task(name="ispstack_access_installation_notifier")
def ispstack_access_installation_notifier():
    django_engine = engines["django"]

    template_subject_access_inst_confirm = (
        SnotifierTemplates.objects.get(
            name="subject_access_inst_confirm"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_ticket_note = (
        SnotifierTemplates.objects.get(
            name="ticket_note"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_general_header = (
        SnotifierTemplates.objects.get(
            name="general_header"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_general_footer = (
        SnotifierTemplates.objects.get(
            name="general_footer"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_access_product_confirmation = (
        SnotifierTemplates.objects.get(
            name="access_inst_confirm"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text

    cpe_list_with_pending_notify = IspAccessCpe.objects.filter(
        ispaccesscpenotifier__sent_instant__year=2999
    ).distinct()

    for cpe in cpe_list_with_pending_notify:
        notifier_list = cpe.ispaccesscpenotifier_set.filter(sent_instant__year=2999)

        template_opts = model_to_dict(cpe)
        template_opts["customer"] = model_to_dict(cpe.customer)
        template_opts["timeframe"] = cpe.timeframe.name
        template_opts["installation_date"] = cpe.installation_date.strftime("%d.%m.%Y")

        for address_line in cpe.ispaccesscpeoptions_set.all():
            template_opts[address_line.attribute.name] = address_line.value

        engine_subject = django_engine.from_string(template_subject_access_inst_confirm)

        engine_note = django_engine.from_string(
            "\r\n".join([template_general_header, template_ticket_note])
        )

        engine_text = django_engine.from_string(
            "\r\n".join(
                [
                    template_general_header,
                    template_access_product_confirmation,
                    template_general_footer,
                ]
            )
        )

        ticket_id = send(
            notifier_list=notifier_list,
            subject=engine_subject.render(template_opts),
            text=engine_text.render(template_opts),
            note=engine_note.render(template_opts),
            ticket_id=cpe.ticket_id,
            main_address=cpe.customer.primary_email,
        )

        cpe.ticket_id = ticket_id
        cpe.save()

        for recipient in notifier_list:
            recipient.ticket_id = ticket_id
            recipient.sent_instant = datetime.now()
            recipient.save()

    return


@shared_task(name="ispstack_access_auth_notifier")
def ispstack_access_auth_notifier():
    django_engine = engines["django"]

    template_subject_access_auth = (
        SnotifierTemplates.objects.get(
            name="subject_access_auth"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_ticket_note = (
        SnotifierTemplates.objects.get(
            name="ticket_note"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_general_header = (
        SnotifierTemplates.objects.get(
            name="general_header"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_general_footer = (
        SnotifierTemplates.objects.get(
            name="general_footer"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_access_auth = (
        SnotifierTemplates.objects.get(
            name="access_auth"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text

    cpe_list_with_pending_notify = IspAccessCpe.objects.filter(auth_sent=False)

    for cpe in cpe_list_with_pending_notify:
        template_opts = model_to_dict(cpe)
        template_opts["customer"] = model_to_dict(cpe.customer)

        for address_line in cpe.ispaccesscpeoptions_set.all():
            template_opts[address_line.attribute.name] = address_line.value

        if not "auth_username" in template_opts or not "auth_password" in template_opts:
            continue

        engine_subject = django_engine.from_string(template_subject_access_auth)

        engine_note = django_engine.from_string(
            "\r\n".join([template_general_header, template_ticket_note])
        )

        engine_text = django_engine.from_string(
            "\r\n".join(
                [template_general_header, template_access_auth, template_general_footer]
            )
        )

        notifier_list = list()
        notify_destination = SnotifierEmailContactZammad(
            ticket_id=cpe.ticket_id,
            email=cpe.master_email,
        )
        notifier_list.append(notify_destination)

        ticket_id = send(
            notifier_list=notifier_list,
            subject=engine_subject.render(template_opts),
            text=engine_text.render(template_opts),
            note=engine_note.render(template_opts),
            ticket_id=cpe.ticket_id,
            main_address=cpe.customer.primary_email,
        )

        cpe.ticket_id = ticket_id
        cpe.auth_sent = True
        cpe.save()

    return
