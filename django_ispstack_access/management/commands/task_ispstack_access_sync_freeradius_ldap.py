from django.core.management.base import BaseCommand

from django_ispstack_access.django_ispstack_access.tasks import (
    ispstack_access_sync_freeradius_ldap,
)


class Command(BaseCommand):
    help = "Run task ispstack_access_sync_freeradius_ldap"

    def handle(self, *args, **options):
        ispstack_access_sync_freeradius_ldap()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
