import random
import string

from django_ispstack_access.django_ispstack_access.models import *


def random_password(pw_length):
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    return "".join(random.choice(chars) for x in range(pw_length))


def generate_radius_username(customer, radius_username_prefix, radius_domain):
    usage_count = 1
    while True:
        radius_username = (
            radius_username_prefix
            + "-"
            + str(customer.external_id)
            + "-"
            + str(usage_count).zfill(4)
            + "@"
            + radius_domain
        )
        try:
            radius_username = IspAccessCpeOptions.objects.get(
                attribute__name="auth_username", value=radius_username
            )
        except IspAccessCpeOptions.DoesNotExist:
            return radius_username

        usage_count += 1
        continue


def add_cpe_option(cpe, option_name, value):
    new_opt = IspAccessCpeOptions(
        cpe=cpe,
        attribute=IspAccessCpeAttributes.objects.get(name=option_name),
        value=value,
    )

    new_opt.save()

    return True
