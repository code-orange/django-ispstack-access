from django.apps import apps
from django.contrib import admin

from . import models

for model in apps.get_app_config("django_ispstack_access").models.values():
    if model == models.IspAccessCpe:
        continue

    admin.site.register(model)


# CPE
class IspAccessCpeOptionsInLine(admin.TabularInline):
    model = models.IspAccessCpeOptions
    extra = 0


class IspAccessCpeNotifierInLine(admin.TabularInline):
    model = models.IspAccessCpeNotifier
    extra = 0


@admin.register(models.IspAccessCpe)
class IspAccessCpeAdmin(admin.ModelAdmin):
    inlines = [IspAccessCpeOptionsInLine, IspAccessCpeNotifierInLine]
