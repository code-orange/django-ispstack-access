from phonenumber_field.modelfields import PhoneNumberField

from django_mdat_customer.django_mdat_customer.models import *
from django_simple_notifier.django_simple_notifier.models import (
    SnotifierEmailContactZammadAbstract,
)


class IspAccessCarrier(models.Model):
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    name = models.CharField(unique=True, max_length=200)
    order_enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "isp_access_carrier"


class IspAccessCarrierLink(models.Model):
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    carrier_article_nr = models.CharField(max_length=20, blank=True, null=True)
    carrier_service_nr = models.CharField(max_length=20, blank=True, null=True)
    carrier = models.ForeignKey(IspAccessCarrier, models.DO_NOTHING)
    fallback_for = models.ForeignKey("self", models.DO_NOTHING, null=True, blank=True)
    subproduct = models.ForeignKey("IspAccessSubProducts", models.DO_NOTHING)
    order_enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)
    internal_article = models.ForeignKey(MdatItems, models.DO_NOTHING)
    product_order = models.BigIntegerField(unique=True)
    bw_kbps_down_max = models.IntegerField(default=0)
    bw_kbps_down_mid = models.IntegerField(default=0)
    bw_kbps_down_min = models.IntegerField(default=0)
    bw_kbps_up_max = models.IntegerField(default=0)
    bw_kbps_up_mid = models.IntegerField(default=0)
    bw_kbps_up_min = models.IntegerField(default=0)

    def __str__(self):
        return self.subproduct.name + " via " + self.carrier.name

    class Meta:
        db_table = "isp_access_carrier_link"
        unique_together = (("carrier", "subproduct"),)


class IspAccessCarrierLinkAttributes(models.Model):
    name = models.CharField(unique=True, max_length=50)
    html_input_type = models.CharField(
        max_length=30, default="text", null=True, blank=True
    )
    default_value = models.CharField(max_length=100, null=True, blank=True)
    form_order = models.IntegerField(default=0)
    form_show = models.BooleanField(default=True)

    class Meta:
        db_table = "isp_access_carrier_link_attributes"


class IspAccessCarrierLinkOptions(models.Model):
    value = models.TextField()
    attribute = models.ForeignKey(
        IspAccessCarrierLinkAttributes, models.DO_NOTHING, db_column="attribute"
    )
    carrier_link = models.ForeignKey(
        IspAccessCarrierLink, models.DO_NOTHING, db_column="carrier_link"
    )

    class Meta:
        db_table = "isp_access_carrier_link_options"
        unique_together = (("attribute", "carrier_link"),)


class IspAccessCarrierTimeframe(models.Model):
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    name = models.CharField(max_length=100)
    carrier = models.ForeignKey(IspAccessCarrier, models.DO_NOTHING)
    enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "isp_access_carrier_timeframe"


class IspAccessProducts(models.Model):
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    name = models.CharField(max_length=100)
    order_enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "isp_access_products"


class IspAccessSubProducts(models.Model):
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    name = models.CharField(max_length=100)
    product = models.ForeignKey(IspAccessProducts, models.DO_NOTHING)
    order_enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "isp_access_sub_products"


class IspAccessCpe(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    product = models.ForeignKey(IspAccessCarrierLink, models.DO_NOTHING)
    best_product = models.ForeignKey(
        IspAccessCarrierLink, models.DO_NOTHING, related_name="+"
    )
    contract_nr = models.CharField(max_length=30)
    installation_date = models.DateField()
    master_email = models.EmailField(max_length=100)
    auth_sent = models.BooleanField(default=False)
    carrier_contract_nr = models.CharField(max_length=30, blank=True, null=True)
    timeframe = models.ForeignKey(IspAccessCarrierTimeframe, models.DO_NOTHING)
    ticket_id = models.CharField(max_length=10, blank=True, null=True)
    activity_id = models.CharField(max_length=50, blank=True, null=True)
    line_id = models.CharField(max_length=50, blank=True, null=True)
    carrier_customer_nr = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return "CPE-" + self.contract_nr

    def save(self, *args, **kwargs):
        if not self.master_email:
            self.master_email = self.customer.primary_email
        super(IspAccessCpe, self).save(*args, **kwargs)

    class Meta:
        db_table = "isp_access_cpe"


class IspAccessCpeNotifier(SnotifierEmailContactZammadAbstract):
    cpe = models.ForeignKey(IspAccessCpe, models.DO_NOTHING, db_column="cpe")
    sent_instant = models.DateTimeField(default=timezone.now)
    sent_reminder = models.DateTimeField(default=timezone.now)
    sent_done = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = "isp_access_cpe_notifier"


class IspAccessCpeAttributes(models.Model):
    name = models.CharField(unique=True, max_length=50)
    html_input_type = models.CharField(
        max_length=30, default="text", null=True, blank=True
    )
    default_value = models.CharField(max_length=100, null=True, blank=True)
    form_order = models.IntegerField(default=0)
    form_show = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "isp_access_cpe_attributes"


class IspAccessCpeOptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    cpe = models.ForeignKey(IspAccessCpe, models.DO_NOTHING, db_column="cpe")
    attribute = models.ForeignKey(
        IspAccessCpeAttributes, models.DO_NOTHING, db_column="attribute"
    )
    value = models.TextField(blank=True)

    class Meta:
        db_table = "isp_access_cpe_options"
        unique_together = (("cpe", "attribute"),)


class IspAccessMdatAttributes(models.Model):
    name = models.CharField(unique=True, max_length=50)
    html_input_type = models.CharField(
        max_length=30, default="text", null=True, blank=True
    )
    default_value = models.CharField(max_length=100, null=True, blank=True)
    form_order = models.IntegerField(default=0)
    form_show = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "isp_access_mdat_attributes"


class IspAccessMdatOptions(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, db_column="customer")
    attribute = models.ForeignKey(
        IspAccessMdatAttributes, models.DO_NOTHING, db_column="attribute"
    )
    value = models.TextField(blank=True)

    class Meta:
        db_table = "isp_access_mdat_options"
        unique_together = (("customer", "attribute"),)


class IspAccessFreeradiusDb(models.Model):
    name = models.CharField(max_length=200, unique=True)
    db_ip = models.CharField(max_length=200)
    db_dbase = models.CharField(max_length=200)
    db_user = models.CharField(max_length=200)
    db_passwd = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = "isp_access_freeradius_db"


class IspAccessCpeMaintenance(models.Model):
    id = models.BigAutoField(primary_key=True)
    cpe = models.ForeignKey(IspAccessCpe, models.DO_NOTHING)
    comment = models.CharField(max_length=250)
    contact_provider_fn = models.CharField(max_length=30)
    contact_provider_ln = models.CharField(max_length=30)
    contact_provider_tel = PhoneNumberField()
    contact_provider_email = models.CharField(max_length=250)
    contact_provider_fax = PhoneNumberField()

    def save(self, *args, **kwargs):
        if self.pk is None:
            now = date.today()
            date_code = now.strftime("%Y%m%d")

            try:
                latest_pk = IspAccessCpeMaintenance.objects.filter(
                    pk__startswith=date_code
                ).latest("pk")
                new_pk = int(latest_pk.pk) + 1
            except IspAccessCpeMaintenance.DoesNotExist:
                new_pk = int(date_code + "000001")

            self.pk = new_pk
        super(IspAccessCpeMaintenance, self).save(*args, **kwargs)

    class Meta:
        db_table = "isp_access_cpe_maintenance"


class IspAccessCpeMaintenanceRemoteId(models.Model):
    id = models.BigAutoField(primary_key=True)
    carrier = models.ForeignKey(IspAccessCarrier, models.DO_NOTHING)
    cpe_maintenance = models.ForeignKey(IspAccessCpeMaintenance, models.DO_NOTHING)
    remote_id = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = "isp_access_cpe_maintenance_rem_id"
        unique_together = (("carrier", "remote_id"),)


class IspAccessCpeMaintenanceLog(models.Model):
    id = models.BigAutoField(primary_key=True)
    carrier = models.ForeignKey(IspAccessCarrier, models.DO_NOTHING)
    cpe_maintenance = models.ForeignKey(IspAccessCpeMaintenance, models.DO_NOTHING)
    log_row = models.PositiveIntegerField(default=0)
    log_text = models.TextField()
    status_code = models.IntegerField(default=200)
    date_added = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = "isp_access_cpe_maintenance_log"


class IspAccessCheckCache(models.Model):
    id = models.BigAutoField(primary_key=True)
    carrier = models.ForeignKey(IspAccessCarrier, models.DO_NOTHING)
    q_hash = models.CharField(max_length=200, unique=True)
    content = models.JSONField()
    date_added = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = "isp_access_check_cache"


class IspAccessMobileSim(models.Model):
    id = models.BigAutoField(primary_key=True)
    carrier = models.ForeignKey(IspAccessCarrier, models.DO_NOTHING)
    date_added = models.DateTimeField(default=timezone.now)
    sim_card_number = models.CharField(max_length=100)
    sim_pin = models.CharField(max_length=20)
    sim_pin2 = models.CharField(max_length=20)
    sim_puk = models.CharField(max_length=20)
    sim_puk2 = models.CharField(max_length=20)

    class Meta:
        db_table = "isp_access_mobile_sim"


class IspAccessMobileSimToCpe(models.Model):
    sim = models.OneToOneField(IspAccessMobileSim, models.DO_NOTHING, primary_key=True)
    cpe = models.OneToOneField(IspAccessCpe, models.DO_NOTHING)

    class Meta:
        db_table = "isp_access_mobile_sim_to_cpe"
