# Generated by Django 2.2 on 2019-04-23 13:58

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_ispstack_access", "0011_add_freeradius_db_connections"),
    ]

    operations = [
        migrations.AddField(
            model_name="ispaccesscpe",
            name="auth_sent",
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name="ispaccesscpe",
            name="master_email",
            field=models.EmailField(default="example@example.com", max_length=100),
        ),
    ]
